# Top End Language Lab Resource Page

Welcome to our resource page. This is where the Top End Language Lab hosts some of our documentation, code, and links to different resources.

---

Quick Links:
- [Interactive Transcription demo](https://wulagi.plothatching.com/)
- [Local Word Discovery v1 - Github repo](https://github.com/abbottLane/local-word-discovery-emnlp)
- [Local Word Discovery with Alignment - Github repo](https://github.com/abbottLane/local-word-discovery)
- [Sparse Transcription reference implementation](https://gitlab.com/cdu-tell/sparse-transcription-data-model-py)
- [Kunwinjku Wordbuilder](https://cdu-tell.gitlab.io/karriborlbme/)
- [Kunwinjku UniMorph Dataset](https://github.com/unimorph/gup)
- [Plains Cree Wordbuilder - Github repo](https://github.com/abbottLane/cree-wordbuilder)
